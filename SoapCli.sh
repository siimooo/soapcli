#!/usr/bin/env bash
# Simple bash script
###############################################################################
set -e          # exit on command errors (so you MUST handle exit codes properly!)
set -E          # pass trap handlers down to subshells
set -o pipefail # capture fail exit codes in piped commands
#set -x         # execution tracing debug messages

# Error handler
on_err() {
	echo ">> ERROR: $?"
	FN=0
	for LN in "${BASH_LINENO[@]}"; do
		[ "${FUNCNAME[$FN]}" = "main" ] && break
		echo ">> ${BASH_SOURCE[$FN]} $LN ${FUNCNAME[$FN]}"
		FN=$(( FN + 1 ))
	done
}
trap on_err ERR

# Exit handler
declare -a EXIT_CMDS
add_exit_cmd() { EXIT_CMDS+="$*;  "; }
on_exit(){ eval "${EXIT_CMDS[@]}"; }
trap on_exit EXIT

# Get command info
CMD_PWD=$(pwd)
CMD="$0"
CMD_DIR="$(cd "$(dirname "$CMD")" && pwd -P)"

# Defaults and command line options
[ "$VERBOSE" ] ||  VERBOSE=
[ "$DEBUG" ]   ||  DEBUG=
[ "$SOAP_ACTION" ]   || SOAP_ACTION=
[ "$URL" ]   || URL=
[ "$FILE" ]   || FILE=
#>>>> PUT YOUR ENV VAR DEFAULTS HERE <<<<

# Basic helpers
out() { echo "$(date +%d.%m.%Y..%H:%M:%S) $*"; }
err() { out "$*" 1>&2; }
vrb() { [ ! "$VERBOSE" ] || out "$@"; }
dbg() { [ ! "$DEBUG" ] || err "$@"; }
die() { err "EXIT: $1" && [ "$2" ] && [ "$2" -ge 0 ] && exit "$2" || exit 1; }

# Show help function to be used below
show_help() {
	awk 'NR>1{print} /^(###|$)/{exit}' "$CMD"
	echo "USAGE: $(basename "$CMD") [arguments]"
	echo "ARGS:"
	MSG=$(awk '/^NARGS=-1; while/,/^esac; done/' "$CMD" | sed -e 's/^[[:space:]]*/  /' -e 's/|/, /' -e 's/)//' | grep '^  -')
	EMSG=$(eval "echo \"$MSG\"")
	echo "$EMSG"
}

# Parse command line options (odd formatting to simplify show_help() above)
NARGS=-1; while [ "$#" -ne "$NARGS" ]; do NARGS=$#; case $1 in
	# SWITCHES
	-h|--help)      # This help message
		show_help; exit 1; ;;
	-d|--debug)     # Enable debugging messages (implies verbose)
		DEBUG=$(( DEBUG + 1 )) && VERBOSE="$DEBUG" && shift && echo "#-INFO: DEBUG=$DEBUG (implies VERBOSE=$VERBOSE)"; ;;
	-v|--verbose)   # Enable verbose messages
		VERBOSE=$(( VERBOSE + 1 )) && shift && echo "#-INFO: VERBOSE=$VERBOSE"; ;;
	# SET SOAP ACTION
	-sa|--soap_action)     # Set a Soap action to a variable
		shift && SOAP_ACTION="$1" && shift && vrb "#-INFO: SOAP_ACTION=$SOAP_ACTION"; ;;
    # SET URL
	-u|--url)     # Set a URL to a variable
		shift && URL="$1" && shift && vrb "#-INFO: URL=$URL"; ;;
	# SET FILE
	-f|--file)     # Set a FILE to a variable
		shift && FILE="$1" && shift && vrb "#-INFO: FILE=$FILE"; ;;
	*)
		break;
esac; done

[ "$DEBUG" ]  &&  set -x

###############################################################################
# Validate
[ $# -gt 0 -a -z "$SOAP_ACTION" ]  &&  SOAP_ACTION="$1"  &&  shift
[ "$SOAP_ACTION" ]  ||  die "You must provide SOAP action!"

[ $# -gt 0 -a -z "$URL" ]  &&  URL="$1"  &&  shift
[ "$URL" ]  ||  die "You must provide URL!"

[ $# -gt 0 -a -z "$FILE" ]  &&  FILE="$1"  &&  shift
[ "$FILE" ]  ||  die "You must provide file!"

[ $# -eq 0 ]  ||  die "ERROR: Unexpected commands!"
###############################################################################

request(){
    curl  -X POST -H "Content-Type: text/xml; charset=utf-8" -H "SOAPAction: \"$SOAP_ACTION\"" -d @$FILE "$URL"
}
request